class TV {
  constructor(canvas) {
    this.calculateParams(canvas)
    this.hookInput(canvas)
  }

  calculateParams(canvas) {
    this.canvas_width = canvas.width = canvas.clientWidth
    this.canvas_height = canvas.height = canvas.clientHeight
    this.context = canvas.getContext('2d', { alpha: false })
    this.context.resetTransform()
    this.visible_rows = this.vborder + this.vdisplay + this.vborder
    let retrace = this.visible_rows
    this.row_height = this.canvas_height / this.visible_rows
    this.full_row_width = this.canvas_width
    this.pixel_width = this.full_row_width / (this.hborder + this.hdisplay + this.hborder)
    this.display_row_width = this.canvas_width - 2 * this.hborder * this.pixel_width
    // Assume 60Hz refresh, should actually store previous ts and recalculate
    // This is however what the browser aims for

    let refresh = 1.0 / 60  // in seconds
    this.row_time = refresh / (this.total_rows + retrace)
  }

  hookInput(canvas) {
    canvas.onmousemove = this.onMouseMove.bind(this)
    canvas.onmousedown = this.onMouseDown.bind(this)
    canvas.onmouseup = this.onMouseUp.bind(this)
  }

  installCartridge(object) {
    this.hborder = object.hborder
    this.vborder = object.vborder
    this.hdisplay = object.hdisplay
    this.vdisplay = object.vdisplay
    this.palette = object.palette
    this.border_color = object.border_color || function(row) { return 0 }
    this.row_callback = object.row_callback || function(row, ts) { return [] }
    this.retrace_callback = object.retrace_callback || function(ts) {}
    this.calculateParams(this.context.canvas)

    if (object.setup) {
      object.setup.bind(this)()
    }
  }

  kill() {
    window.cancelAnimationFrame(this.cbid)
  }

  onMouseMove(event) {
    // [0..1] where 0 is top edge, 1 is bottom edge
    this.paddle1 = event.offsetY / this.canvas_height
  }

  onMouseDown(event) {
    this.button1 = 1
  }

  onMouseUp(event) {
    this.button1 = 0
  }

  showFps() {
    if (!this.prev) return
    let fps = 1000.0 / (this.start - this.prev)
    this.context.font = '24px sans-serif'
    this.context.fillStyle = '#fff'
    this.context.fillText(fps.toFixed() + '', 0, 24)
  }

  paint(ts) {
    this.prev = this.start
    this.start = ts
    // this.paint_int = window.setInterval(this.paint_line, this.row_time)
    // active loop, burns cpu
    for (var i = 0; i < this.vborder; i++) this.paintBorder(i)
    for (i = 0; i < this.vdisplay; i++) this.paintDisplay(i)
    for (i = 0; i < this.vborder; i++) this.paintBorder(i + this.vborder + this.vdisplay)
    this.retrace_callback()
    this.showFps()
  }

  paintDisplay(row) {
    let y = (row + this.vborder) * this.row_height
    this.paintPixels(row, y, this.row_callback(row, this.start))
  }

  paintBorder(row) {
    this.context.fillStyle = this.context.strokeStyle = this.palette[this.border_color(row)]
    this.context.fillRect(0, Math.floor(row * this.row_height), Math.ceil(this.full_row_width), Math.ceil(this.row_height))
  }

  paintPixels(row, top, colors) {
    // colors are an array of size exactly hdisplay (extra entries ignored)
    // indexes into the palette array
    let left = this.hborder * this.pixel_width
    this.context.fillStyle = this.context.strokeStyle = this.palette[this.border_color(row + this.vborder)]
    this.context.fillRect(0, Math.floor(top), Math.ceil(this.hborder * this.pixel_width), Math.ceil(this.row_height))
    for (var i = 0; i < this.hdisplay; i++) {
      this.context.fillStyle = this.context.strokeStyle = this.palette[colors[i]]
      this.context.fillRect(Math.floor(left + i * this.pixel_width), Math.floor(top), Math.ceil(this.pixel_width), Math.ceil(this.row_height))
    }
    this.context.fillStyle = this.context.strokeStyle = this.palette[this.border_color(row + this.vborder)]
    this.context.fillRect(Math.floor(left + this.pixel_width * this.hdisplay), Math.floor(top), Math.ceil(this.hborder * this.pixel_width), Math.ceil(this.row_height))
  }

  // cartridge-defined
  // vborder: 8,
  // vdisplay: 32,

  // hborder: 4,
  // hdisplay: 32,

  // palette: ['#000', '#fff'], // B/W
  // palette: ['#000', '#00a', '#0a0', '#0aa', '#a00', '#a0a', '#a50', '#aaa',
  //           '#555', '#55f', '#5f5', '#5ff', '#f55', '#f5f', '#ff5', '#fff'], // VGA

  // cartridge-defined, returns pallette index
  border_color(row) {
    return 0
  }

  // cartridge-defined
  // NOTE: maybe shouldn't return, but write entries into some register?
  // or call a function to do so
  row_callback(row, ts) {
    return [0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0]
  }

  // cartridge-defined
  // For proper games should be on another thread, but we need it to share cpu with painting here
  retrace_callback(ts) {
  }
}

let pong = {
  vborder: 0,
  vdisplay: 160,
  hborder: 0,
  hdisplay: 192,

  palette: ['#000', '#fff'],

  setup() {
    this.p1_paddle = 64
    this.p2_paddle = 64
    this.ball_x = 96
    this.ball_y = 80
    this.ball_vx = 0
    this.ball_vy = 0
    this.p1_score = 0
    this.p2_score = 0
  },

  row_callback(row, ts) {
    let r = [0, 0]
    if (row >= this.p1_paddle && row <= this.p1_paddle + 16) {
      r.push(1, 1, 1, 1)
    } else {
      r.push(0, 0, 0, 0)
    }

    if (row <= this.ball_y || row >= this.ball_y + 2) {
      for (var i = 0; i < 182; i++) r.push(0)
    } else {
      for (i = 0; i < this.ball_x; i++) r.push(0)
      r.push(1, 1)
      for (i = this.ball_x + 2; i < 180; i++) r.push(0)
    }

    if (row >= this.p2_paddle && row <= this.p2_paddle + 16) {
      r.push(1, 1, 1, 1)
    } else {
      r.push(0, 0, 0, 0)
    }
    r.push(0, 0)
    return r
  },

  retrace_callback(ts) {
    this.p1_paddle = Math.floor(this.paddle1 * 160)
    this.p2_paddle = this.ball_y - 8

    if (this.button1 !=+ 0 && this.ball_vx == 0 && this.ball_vy === 0) {
      this.ball_vx = 4 - Math.random() * 8
      this.ball_vy = 4 - Math.random() * 8
    }

    this.ball_y += this.ball_vy; this.ball_x += this.ball_vx
    if (this.ball_y <= 0 || this.ball_y >= 160) {
      this.ball_vy = -this.ball_vy
    }

    if (this.ball_x <= 0 || this.ball_x >= 192) {
      this.ball_vx = -this.ball_vx
    }
  }
}

function setupTV(selector) {
  let tv = new TV(document.querySelector(selector), document.querySelector('#fps'))
  tv.installCartridge(pong)

  let repaint = function(ts) {
    tv.paint(ts)
    tv.cbid = window.requestAnimationFrame(repaint)
  }

  tv.cbid = window.requestAnimationFrame(repaint)
  return tv
}

document.onreadystatechange = function() {
  if (document.readyState === 'interactive') {
    window.tv = setupTV('#tv')
  }

  document.querySelector('#kill').onclick = function() {
    window.tv.kill()
  }

  document.querySelector('#frame').onclick = function() {
    window.tv.paint(0)
  }
}
